

const BACKEND_URL = "http://localhost:5050/books";

async function fetchData(){
  const request = new Request (BACKEND_URL, {method: 'GET'})
  const response = await fetch(request);
  const result = await response.json();

  document.getElementById('table').innerHTML = "";

  let htmlCode = '<div class="container"><div class="row">';
    for (var i = 0; i < result.length; i++) {
      htmlCode += '<div class="col-lg-3 border border-info rounded">';
      htmlCode += '<br><center> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="'+ getRandomImage() +'" width="115" height="160" class="mr-4 rounded" alt="picture of book"></center>';
      htmlCode += '<br>' + result[i].title +'</br>';
      htmlCode += '<br>' + result[i].author +'</br>';
      htmlCode += '<br>' + result[i].price + '</br>';
      htmlCode += '<br> <button type="button" class="btn btn-danger" onclick=removeData(\''+ result[i]._id +'\')>Delete</button> </br>';
      htmlCode += '<br><button type="button" class="btn btn-secondary" onclick=putData>Update</button></br>';
      htmlCode += '</div>';
  }
  htmlCode+= '</div></div>';
  document.getElementById('table').innerHTML += htmlCode;
}


async function postData(){
  const data = {title: '', author: '', price: 0}

  data.title = document.getElementById('title').value;
  data.author = document.getElementById('author').value;
  data.price = document.getElementById('price').value;
   console.log(data)

  const request = new Request(BACKEND_URL, {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(data)
  });


  const response = await fetch(request);
  if (response.status === 400) {
    console.log("It failed! Error: ")
  }
  else if (response.status === 500){
    console.log("Server Error")
  }
  const result = await response.json();
  console.log("Record inserted sucessfully")
  console.log(result);

  fetchData();
  }

async function removeData(id = ''){

  const data = {id}

  const request = new Request(BACKEND_URL +'/'+ id , {
    method: 'DELETE',
    headers: {'Accept': 'application/json',
              'Content-Type': 'application/json'},
    body: JSON.stringify(data)
  });

  const response = await fetch(request);
    if (response.status === 400) {
      console.log("It Failed!");
  }

    if (response.status === 500){
      console.log("Server Error!")
    }

  const result = await response.json();
    console.log("Record Deleted Successfully");

  fetchData();
}

async function putData() {

  const data = {title: '', author: '', price: 0}

  data.title = document.getElementById('title').value;
  data.author = document.getElementById('author').value;
  data.price = document.getElementById('price').value;
   console.log(data)

   const request = new Request(BACKEND_URL +'/'+ id, {
     method : 'PUT',
     headers: {'Accept': 'application/json',
               'Content-Type': 'application/json'},
     body: JSON.stringify(data)
   });

   const response = await fetch(request);
    if (response.status === 400){
      console.log("It Failed!");
    }

    if (response.status === 500){
      console.log("Server Error!")
    }

  const result = await response.json();
    console.log("Recond Successfully Updated");

  fetchData();
}
